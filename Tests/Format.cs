﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Xunit;
using Newtonsoft.Json.Linq;

using EasyMsgPack;

namespace Tests
{
	public class Format
	{
		[Fact]
		public void Suite()
		{
			string file = File.ReadAllText("msgpack-test-suite.json");
			dynamic testSuite = JObject.Parse(file);
			foreach (JProperty? entry in testSuite)
			{
				string name = entry!.Name;
				dynamic tests = entry!.Value;
				foreach (dynamic? test in tests)
				{
					if (test == null) throw new Exception();

					string[] strEncodings = ((JArray)test["msgpack"]).ToObject<string[]>()!;
					byte[][] encodings = strEncodings.Select(FromString).ToArray();
					var kvp = (JProperty)((IList<JToken>)test)[0];
					string type = kvp.Name;
					dynamic value = kvp.Value;
					if (!(type switch {
						"binary" => FromString((string)value),
						"bignum" => null,
						"timestamp" => TryOrDefault(() => new DateTime(
							((long)value[0] * 10000000)
							+ ((long)value[1] / 100)
							+ DateTime.UnixEpoch.Ticks
						), (DateTime?)null),
						"ext" => new MsgCustom(
							(sbyte)value[0],
							FromString((string)value[1])
						),
						_ => ToMsg(value),
					} is Msg expected)) continue;

					Assert.All(encodings, e => Assert.Equal(expected, Msg.Unpack(e)));
					Assert.Contains(expected.Pack(), encodings);
				}
			}
		}

		static Msg ToMsg(JToken json)
		{
			if (json.Type == JTokenType.Null) return MsgNil.Nil;
			else if (json.Type == JTokenType.Boolean) return (bool)json;
			else if (json.Type == JTokenType.String) return ((string)json)!;
			else if (json.Type == JTokenType.Integer || json.Type == JTokenType.Float) return (double)json;
			else if (json is JArray a) return a.Select(ToMsg).ToArray();
			else if (json is JObject o) return Enumerable.ToDictionary<KeyValuePair<string, JToken>, Msg, Msg>(o!, o => o.Key, o => ToMsg(o.Value));
			else throw new Exception();
		}

		static byte[] FromString(string s) => s.Length == 0 ? Array.Empty<byte>() : s.Split('-').Select(b => Convert.ToByte(b, 16)).ToArray();

		static T TryOrDefault<T>(Func<T> toTry, T defaultValue)
		{
			try { return toTry(); }
			catch { return defaultValue; }
		}
	}
}
