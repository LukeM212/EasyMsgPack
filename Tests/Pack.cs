using System;

using Xunit;

using EasyMsgPack;

namespace Tests
{
	public class Packing
	{
		[Fact]
		public void PackNil()
		{
			AssertPack(MsgNil.Nil, 1); // nil
		}

		[Fact]
		public void PackBool()
		{
			AssertPack(true, 1); // true
			AssertPack(false, 1); // false
		}

		[Fact]
		public void PackInt()
		{
			AssertPack(0, 1); // zero
			AssertPack(0b1111111, 1); // positive fixnum
			AssertPack(0b10000000, 2); // not positive fixnum
			AssertPack(-0b100000, 1); // negative fixnum
			AssertPack(-0b100001, 2); // not negative fixnum
			AssertPack(byte.MaxValue, 2); // uint 8
			AssertPack(byte.MaxValue + (ushort)1, 3); // not uint 8
			AssertPack(ushort.MaxValue, 3); // uint 16
			AssertPack(ushort.MaxValue + (uint)1, 5); // not uint 16
			AssertPack(uint.MaxValue, 5); // uint 32
			AssertPack(uint.MaxValue + (ulong)1, 9); // not uint 32
			AssertPack(ulong.MaxValue, 9); // uint 64
			AssertPack(sbyte.MinValue, 2); // int 8
			AssertPack(sbyte.MinValue - (short)1, 3); // not int 8
			AssertPack(short.MinValue, 3); // int 16
			AssertPack(short.MinValue - (int)1, 5); // not int 16
			AssertPack(int.MinValue, 5); // int 32
			AssertPack(int.MinValue - (long)1, 9); // not int 32
			AssertPack(long.MinValue, 9); // int 64
		}

		[Fact]
		public void PackFloat()
		{
			AssertPack(float.Epsilon, 5); // float Epsilon
			AssertPack(float.MaxValue, 5); // float MaxValue
			AssertPack(float.MinValue, 5); // float MinValue
			AssertPack(float.NaN, 5); // float NaN
			AssertPack(float.NegativeInfinity, 5); // float NegativeInfinity
			AssertPack(float.PositiveInfinity, 5); // float PositiveInfinity

			AssertPack(double.Epsilon, 9); // double Epsilon
			AssertPack(double.MaxValue, 9); // double MaxValue
			AssertPack(double.MinValue, 9); // double MinValue
			// AssertPack(double.NaN, 9); // double NaN
			// AssertPack(double.NegativeInfinity, 9); // double NegativeInfinity
			// AssertPack(double.PositiveInfinity, 9); // double PositiveInfinity
		}

		[Fact]
		public void PackStr()
		{
			AssertPack(new string('a', 0), 1 + 0); // empty string
			AssertPack(new string('a', 31), 1 + 31); // fixstr
			AssertPack(new string('a', 31 + 1), 2 + 31 + 1); // not fixstr
			AssertPack(new string('a', byte.MaxValue), 2 + byte.MaxValue); // str 8
			AssertPack(new string('a', byte.MaxValue + 1), 3 + byte.MaxValue + 1); // not str 8
			AssertPack(new string('a', ushort.MaxValue), 3 + ushort.MaxValue); // str 16
			AssertPack(new string('a', ushort.MaxValue + 1), 5 + ushort.MaxValue + 1); // not str 16
			AssertPack(new string('a', 12345678), 5 + 12345678); // str 32

			AssertPack(new string('�', 15), 1 + 15 * 2); // fixstr
			AssertPack(new string('�', 16), 2 + 16 * 2); // not fixstr
		}

		[Fact]
		public void PackBin()
		{
			AssertPack(Array.Empty<byte>(), 2); // empty byte array
			AssertPack(new byte[byte.MaxValue], 2 + byte.MaxValue); // bin 8
			AssertPack(new byte[byte.MaxValue + 1], 3 + byte.MaxValue + 1); // not bin 8
			AssertPack(new byte[ushort.MaxValue], 3 + ushort.MaxValue); // bin 16
			AssertPack(new byte[ushort.MaxValue + 1], 5 + ushort.MaxValue + 1); // not bin 16
			AssertPack(new byte[12345678], 5 + 12345678); // bin 32
		}

		[Fact]
		public void PackArray()
		{
			AssertPack(new MsgArray { }, 1); // empty array
			AssertPack(FillArray(15, MsgNil.Nil), 1 + 15); // fixarray
			AssertPack(FillArray(15 + 1, MsgNil.Nil), 3 + 15 + 1); // not fixarray
			AssertPack(FillArray(ushort.MaxValue, MsgNil.Nil), 3 + ushort.MaxValue); // array 16
			AssertPack(FillArray(ushort.MaxValue + 1, MsgNil.Nil), 5 + ushort.MaxValue + 1); // not array 16
			AssertPack(FillArray(12345678, MsgNil.Nil), 5 + 12345678); // array 32
		}

		[Fact]
		public void PackMap()
		{
			static Msg CreateMap(uint length, Msg value)
			{
				int digits = length.ToString().Length;

				MsgMap map = new MsgMap();

				for (uint i = 0; i < length; i++)
					map.Add(i.ToString().PadLeft(digits, '0'), value);

				return map;
			}

			AssertPack(new MsgMap { }, 1); // empty map
			AssertPack(CreateMap(15, MsgNil.Nil), 1 + 15 * 4); // fixmap
			AssertPack(CreateMap(15 + 1, MsgNil.Nil), 3 + (15 + 1) * 4); // not fixmap
			AssertPack(CreateMap(ushort.MaxValue, MsgNil.Nil), 3 + ushort.MaxValue * 7); // map 16
			AssertPack(CreateMap(ushort.MaxValue + 1, MsgNil.Nil), 5 + (ushort.MaxValue + 1) * 7); // not map 16
			AssertPack(CreateMap(12345678, MsgNil.Nil), 5 + 12345678 * 10); // map 32
		}

		[Fact]
		public void PackTimestamp()
		{
			AssertPack(DateTime.Today, 6); // timestamp 32
			AssertPack(DateTime.Now, 10); // timestamp 64
			AssertPack(DateTime.UnixEpoch.AddTicks(-0x123456789), 15); // timestamp 96
		}

		static T[] FillArray<T>(uint length, T value)
		{
			T[] array = new T[length];
			Array.Fill(array, value);
			return array;
		}

		static void AssertPack(Msg msg)
		{
			byte[] data = msg.Pack();

			Msg newMsg = Msg.Unpack(data);

			Assert.Equal(msg.Value, newMsg.Value);
		}

		static void AssertPack(Msg msg, uint length)
		{
			byte[] data = msg.Pack();

			Assert.Equal(length, (uint)data.Length);

			Msg newMsg = Msg.Unpack(data);

			Assert.Equal(msg.Value, newMsg.Value);
		}
	}
}
