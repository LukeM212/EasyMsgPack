﻿using System;
using System.Collections.Generic;

using Xunit;

using EasyMsgPack;

namespace Tests
{
	public class Casts
	{
		[Fact]
		public void CastBool()
		{
			MsgBool msgBool = true;
			Assert.Equal<bool>(true, msgBool);

			Msg msg = true;
			Assert.Equal<bool>(true, (bool)msg);
		}

		[Fact]
		public void CastInt()
		{
			MsgInt msgInt = 7;

			Assert.Equal<byte>(7, (byte)msgInt);
			Assert.Equal<sbyte>(7, (sbyte)msgInt);
			Assert.Equal<ushort>(7, (ushort)msgInt);
			Assert.Equal<short>(7, (short)msgInt);
			Assert.Equal<uint>(7, (uint)msgInt);
			Assert.Equal<int>(7, (int)msgInt);
			Assert.Equal<ulong>(7, (ulong)msgInt);
			Assert.Equal<long>(7, (long)msgInt);

			Assert.Equal<float>(7, (float)msgInt);
			Assert.Equal<double>(7, (double)msgInt);


			Msg msg = 7;

			Assert.Equal<byte>(7, (byte)msg);
			Assert.Equal<sbyte>(7, (sbyte)msg);
			Assert.Equal<ushort>(7, (ushort)msg);
			Assert.Equal<short>(7, (short)msg);
			Assert.Equal<uint>(7, (uint)msg);
			Assert.Equal<int>(7, (int)msg);
			Assert.Equal<ulong>(7, (ulong)msg);
			Assert.Equal<long>(7, (long)msg);

			Assert.Equal<float>(7, (float)msg);
			Assert.Equal<double>(7, (double)msg);
		}

		[Fact]
		public void CastFloat()
		{
			MsgFloat msgFloat = 7.0;

			Assert.Equal<byte>(7, (byte)msgFloat);
			Assert.Equal<sbyte>(7, (sbyte)msgFloat);
			Assert.Equal<ushort>(7, (ushort)msgFloat);
			Assert.Equal<short>(7, (short)msgFloat);
			Assert.Equal<uint>(7, (uint)msgFloat);
			Assert.Equal<int>(7, (int)msgFloat);
			Assert.Equal<ulong>(7, (ulong)msgFloat);
			Assert.Equal<long>(7, (long)msgFloat);

			Assert.Equal<float>(7, (float)msgFloat);
			Assert.Equal<double>(7, msgFloat);


			Msg msg = 7.0;

			Assert.Equal<byte>(7, (byte)msg);
			Assert.Equal<sbyte>(7, (sbyte)msg);
			Assert.Equal<ushort>(7, (ushort)msg);
			Assert.Equal<short>(7, (short)msg);
			Assert.Equal<uint>(7, (uint)msg);
			Assert.Equal<int>(7, (int)msg);
			Assert.Equal<ulong>(7, (ulong)msg);
			Assert.Equal<long>(7, (long)msg);

			Assert.Equal<float>(7, (float)msg);
			Assert.Equal<double>(7, (double)msg);
		}

		[Fact]
		public void CastStr()
		{
			MsgStr msgStr = "test";
			Assert.Equal<string>("test", msgStr);

			Msg msg = "test";
			Assert.Equal<string>("test", (string)msg);
		}

		[Fact]
		public void CastBin()
		{
			MsgBin msgBin = new byte[] { 0xFF, 0x12, 0x56, 0x34 };
			Assert.Equal<byte[]>(new byte[] { 0xFF, 0x12, 0x56, 0x34 }, msgBin);

			Msg msg = new byte[] { 0xFF, 0x12, 0x56, 0x34 };
			Assert.Equal<byte[]>(new byte[] { 0xFF, 0x12, 0x56, 0x34 }, (byte[])msg);
		}

		[Fact]
		public void CastArray()
		{
			MsgArray msgArray = new Msg[] { MsgNil.Nil, 345, false, "hi" };
			Assert.Equal<List<Msg>>(new List<Msg> { MsgNil.Nil, 345, false, "hi" }, msgArray);
			Assert.Equal<Msg[]>(new Msg[] { MsgNil.Nil, 345, false, "hi" }, (Msg[])msgArray);

			Msg msg = new Msg[] { MsgNil.Nil, 345, false, "hi" };
			Assert.Equal<List<Msg>>(new List<Msg> { MsgNil.Nil, 345, false, "hi" }, (List<Msg>)msg);
			Assert.Equal<Msg[]>(new Msg[] { MsgNil.Nil, 345, false, "hi" }, (Msg[])msg);
		}

		[Fact]
		public void CastMap()
		{
			MsgMap msgMap = new Dictionary<Msg, Msg> { { "a", MsgNil.Nil }, { "b", 345 }, { "c", false }, { "d", "hi" } };
			Assert.Equal<Dictionary<Msg, Msg>>(new Dictionary<Msg, Msg> { { "a", MsgNil.Nil }, { "b", 345 }, { "c", false }, { "d", "hi" } }, msgMap);

			Msg msg = new Dictionary<Msg, Msg> { { "a", MsgNil.Nil }, { "b", 345 }, { "c", false }, { "d", "hi" } };
			Assert.Equal<Dictionary<Msg, Msg>>(new Dictionary<Msg, Msg> { { "a", MsgNil.Nil }, { "b", 345 }, { "c", false }, { "d", "hi" } }, (Dictionary<Msg, Msg>)msg);
		}

		[Fact]
		public void CastTimestamp()
		{
			MsgTimestamp msgTimestamp = DateTime.Parse("2000-06-21T11:22:33.444Z");
			Assert.Equal<DateTime>(DateTime.Parse("2000-06-21T11:22:33.444Z"), msgTimestamp);

			Msg msg = DateTime.Parse("2000-06-21T11:22:33.444Z");
			Assert.Equal<DateTime>(DateTime.Parse("2000-06-21T11:22:33.444Z"), (DateTime)msg);
		}
	}
}
