# EasyMsgPack

An easy-to-use implementation of the MessagePack serialisation format.

You can install this through NuGet by searching for [EasyMsgPack](https://www.nuget.org/packages/EasyMsgPack/).
