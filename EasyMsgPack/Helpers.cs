﻿using System;

namespace EasyMsgPack
{
	internal static class Helpers
	{
		public static int DivMod(int num, int den, out int mod)
		{
			int div = num / den;
			mod = num % den;
			if (mod < 0)
			{
				div -= 1;
				mod += den;
			}
			return div;
		}

		public static uint DivMod(uint num, uint den, out uint mod)
		{
			uint div = num / den;
			mod = num % den;
			if (mod < 0)
			{
				div -= 1;
				mod += den;
			}
			return div;
		}

		public static long DivMod(long num, long den, out long mod)
		{
			long div = num / den;
			mod = num % den;
			if (mod < 0)
			{
				div -= 1;
				mod += den;
			}
			return div;
		}

		public static ulong DivMod(ulong num, ulong den, out ulong mod)
		{
			ulong div = num / den;
			mod = num % den;
			if (mod < 0)
			{
				div -= 1;
				mod += den;
			}
			return div;
		}

		public static ushort Rev(ushort x) => (ushort)(
			(x << 8) |
			(x >> 8)
		);
		public static short Rev(short x) => (short)Rev((ushort)x);

		public static uint Rev(uint x) => (uint)(
			 (x << 24) |
			((x <<  8) & 0x00FF0000) |
			((x >>  8) & 0x0000FF00) |
			 (x >> 24)
		);
		public static int Rev(int x) => (int)Rev((uint)x);

		public static ulong Rev(ulong x) => (ulong)(
			 (x << 56) |
			((x << 40) & 0x00FF000000000000) |
			((x << 24) & 0x0000FF0000000000) |
			((x <<  8) & 0x000000FF00000000) |
			((x >>  8) & 0x00000000FF000000) |
			((x >> 24) & 0x0000000000FF0000) |
			((x >> 40) & 0x000000000000FF00) |
			 (x >> 56)
		);
		public static long Rev(long x) => (long)Rev((ulong)x);
	}
}
