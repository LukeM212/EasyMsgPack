﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace EasyMsgPack
{
	public abstract class Msg
	{
		public static Msg Unpack(byte[] data)
		{
			using MemoryStream stream = new MemoryStream(data);
			return Unpack(stream);
		}
		public static Msg Unpack(Stream stream)
		{
			using BinaryReader reader = new BinaryReader(stream, Encoding.UTF8, true);
			return Unpack(reader);
		}
		public static Msg Unpack(BinaryReader reader)
		{
			byte type = reader.ReadByte();

			if (type <= 0b11001001)
			{
				if (type <= 0b101_11111)
				{
					if (type <= 0b0_1111111) return MsgInt.Unpack(type, reader);
					else if (type <= 0b1000_1111) return MsgMap.Unpack(type, reader);
					else if (type <= 0b1001_1111) return MsgArray.Unpack(type, reader);
					else return MsgStr.Unpack(type, reader);
				}
				else if (type == 0b11000000) return MsgNil.Unpack(type, reader);
				else if (type == 0b11000001) throw new NotImplementedException();
				else if (type <= 0b11000011) return MsgBool.Unpack(type, reader);
				else if (type <= 0b11000110) return MsgBin.Unpack(type, reader);
				else return MsgExt.Unpack(type, reader);
			}
			else if (type <= 0b11010011)
			{
				if (type <= 0b11001011) return MsgFloat.Unpack(type, reader);
				else return MsgInt.Unpack(type, reader);
			}
			else if (type <= 0b11011011)
			{
				if (type <= 0b11011000) return MsgExt.Unpack(type, reader);
				else return MsgStr.Unpack(type, reader);
			}
			else if (type <= 0b11011111)
			{
				if (type <= 0b11011101) return MsgArray.Unpack(type, reader);
				else return MsgMap.Unpack(type, reader);
			}
			else return MsgInt.Unpack(type, reader);
		}

		public byte[] Pack()
		{
			using MemoryStream stream = new MemoryStream();
			this.Pack(stream);
			return stream.ToArray();
		}
		public void Pack(Stream stream)
		{
			using BinaryWriter writer = new BinaryWriter(stream, Encoding.UTF8, true);
			Pack(new BinaryWriter(stream));
		}
		public abstract void Pack(BinaryWriter stream);


		public abstract object? Value { get; }


		public static implicit operator Msg(bool value) => (MsgBool)value;

		public static implicit operator Msg(sbyte value) => (MsgInt)value;
		public static implicit operator Msg(byte value) => (MsgInt)value;
		public static implicit operator Msg(short value) => (MsgInt)value;
		public static implicit operator Msg(ushort value) => (MsgInt)value;
		public static implicit operator Msg(int value) => (MsgInt)value;
		public static implicit operator Msg(uint value) => (MsgInt)value;
		public static implicit operator Msg(long value) => (MsgInt)value;
		public static implicit operator Msg(ulong value) => (MsgInt)value;

		public static implicit operator Msg(float value) => (MsgFloat)value;
		public static implicit operator Msg(double value) => (MsgFloat)value;

		public static implicit operator Msg(string value) => (MsgStr)value;

		public static implicit operator Msg(byte[] value) => (MsgBin)value;

		public static implicit operator Msg(List<Msg> value) => (MsgArray)value;
		public static implicit operator Msg(Msg[] value) => (MsgArray)value;

		public static implicit operator Msg(Dictionary<Msg, Msg> value) => (MsgMap)value;

		public static implicit operator Msg(DateTime value) => (MsgTimestamp)value;


		public static explicit operator bool(Msg msg) => (bool)(MsgBool)msg;

		public static explicit operator sbyte(Msg msg) => (sbyte)(MsgNumber)msg;
		public static explicit operator byte(Msg msg) => (byte)(MsgNumber)msg;
		public static explicit operator short(Msg msg) => (short)(MsgNumber)msg;
		public static explicit operator ushort(Msg msg) => (ushort)(MsgNumber)msg;
		public static explicit operator int(Msg msg) => (int)(MsgNumber)msg;
		public static explicit operator uint(Msg msg) => (uint)(MsgNumber)msg;
		public static explicit operator long(Msg msg) => (long)(MsgNumber)msg;
		public static explicit operator ulong(Msg msg) => (ulong)(MsgNumber)msg;

		public static explicit operator float(Msg msg) => (float)(MsgNumber)msg;
		public static explicit operator double(Msg msg) => (double)(MsgNumber)msg;

		public static explicit operator string(Msg msg) => (string)(MsgStr)msg;

		public static explicit operator byte[](Msg msg) => (byte[])(MsgBin)msg;

		public static explicit operator List<Msg>(Msg msg) => (List<Msg>)(MsgArray)msg;
		public static explicit operator Msg[](Msg msg) => (Msg[])(MsgArray)msg;

		public static explicit operator Dictionary<Msg, Msg>(Msg msg) => (Dictionary<Msg, Msg>)(MsgMap)msg;

		public static explicit operator DateTime(Msg msg) => (DateTime)(MsgTimestamp)msg;
	}
}
