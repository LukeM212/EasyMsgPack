﻿using System.IO;

namespace EasyMsgPack.Extensions
{
	public static class Extensions
	{
		public static Msg Unpack(this Stream stream) => Msg.Unpack(stream);

		public static void Pack(this Stream stream, Msg msg) => msg.Pack(stream);
	}
}
