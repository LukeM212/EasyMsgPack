﻿using System;
using System.IO;
using System.Linq;

namespace EasyMsgPack
{
	using static Helpers;

	public class MsgBin : Msg
	{
		private readonly byte[] value;

		public MsgBin(byte[] value)
		{
			this.value = value;
		}

		public static implicit operator MsgBin(byte[] value) => new MsgBin(value);

		public override void Pack(BinaryWriter stream)
		{
			uint length = (uint)value.Length;

			if (length <= byte.MaxValue)
			{
				stream.Write((byte)0xC4);
				stream.Write((byte)length);
			}
			else if (length <= ushort.MaxValue)
			{
				stream.Write((byte)0xC5);
				stream.Write(Rev((ushort)length));
			}
			else
			{
				stream.Write((byte)0xC6);
				stream.Write(Rev((uint)length));
			}

			stream.Write(value);
		}

		internal static Msg Unpack(byte type, BinaryReader reader)
		{
			uint length;

			if (type == 0xC4) length = reader.ReadByte();
			else if (type == 0xC5) length = Rev(reader.ReadUInt16());
			else length = Rev(reader.ReadUInt32());

			byte[] data = reader.ReadBytes((int)length);
			return new MsgBin(data);
		}

		public override object? Value => value;

		public byte this[int index] => value[index];

		public static implicit operator byte[](MsgBin msg) => msg.value;

		public override string ToString() => throw new NotImplementedException();

		public int Length => value.Length;

		public override bool Equals(object obj) => obj is MsgBin msg && this == msg;
		public static bool operator !=(MsgBin a, MsgBin b) => !(a == b);
		public static bool operator ==(MsgBin a, MsgBin b)
		{
			return Enumerable.SequenceEqual(a.value, b.value);
		}

		public override int GetHashCode()
		{
			return value.Aggregate(
				unchecked((int)0x5728FE8B),
				(total, next) => HashCode.Combine(total, next)
			);
		}
	}
}
