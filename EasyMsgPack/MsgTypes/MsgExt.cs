using System.IO;

namespace EasyMsgPack
{
	using static Helpers;

	public abstract class MsgExt : Msg
	{
		public abstract sbyte Type { get; }
		public abstract byte[] Data { get; }

		public override void Pack(BinaryWriter stream)
		{
			sbyte type = Type;
			byte[] data = Data;

			uint length = (uint)data.Length;

			if (length == 1) stream.Write((byte)0xD4);
			else if (length == 2) stream.Write((byte)0xD5);
			else if (length == 4) stream.Write((byte)0xD6);
			else if (length == 8) stream.Write((byte)0xD7);
			else if (length == 16) stream.Write((byte)0xD8);
			else if (length <= byte.MaxValue)
			{
				stream.Write((byte)0xC7);
				stream.Write((byte)length);
			}
			else if (length <= ushort.MaxValue)
			{
				stream.Write((byte)0xC8);
				stream.Write(Rev((ushort)length));
			}
			else
			{
				stream.Write((byte)0xC9);
				stream.Write(Rev((uint)length));
			}

			stream.Write((sbyte)type);

			stream.Write(data);
		}

		internal static Msg Unpack(byte type, BinaryReader reader)
		{
			uint length;

			if (type <= 0xC9)
			{
				if (type == 0xC7) length = reader.ReadByte();
				else if (type == 0xC8) length = Rev(reader.ReadUInt16());
				else length = Rev(reader.ReadUInt32());
			}
			else
			{
				if (type == 0xD4) length = 1;
				else if (type == 0xD5) length = 2;
				else if (type == 0xD6) length = 4;
				else if (type == 0xD7) length = 8;
				else length = 16;
			}

			sbyte extType = reader.ReadSByte();
			byte[] data = reader.ReadBytes((int)length);

			return extType switch {
				-1 => new MsgTimestamp(extType, data),
				_ => new MsgCustom(extType, data),
			};
		}
	}
}
