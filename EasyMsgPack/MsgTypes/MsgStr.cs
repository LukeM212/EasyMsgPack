﻿using System;
using System.IO;
using System.Text;

namespace EasyMsgPack
{
	using static Helpers;

	public class MsgStr : Msg
	{
		private readonly string value;

		public MsgStr(string value)
		{
			this.value = value;
		}

		public static implicit operator MsgStr(string value) => new MsgStr(value);

		public override void Pack(BinaryWriter stream)
		{
			byte[] data = Encoding.UTF8.GetBytes(value);
			uint length = (uint)data.Length;

			if (length <= 0b11111) stream.Write((byte)(length | 0b101_00000));
			else if (length <= byte.MaxValue)
			{
				stream.Write((byte)0xD9);
				stream.Write((byte)length);
			}
			else if (length <= ushort.MaxValue)
			{
				stream.Write((byte)0xDA);
				stream.Write(Rev((ushort)length));
			}
			else
			{
				stream.Write((byte)0xDB);
				stream.Write(Rev((uint)length));
			}

			stream.Write(data);
		}

		internal static Msg Unpack(byte type, BinaryReader reader)
		{
			uint length;

			if (type <= 0b101_11111) length = type & (uint)0b11111;
			else if (type == 0xD9) length = reader.ReadByte();
			else if (type == 0xDA) length = Rev(reader.ReadUInt16());
			else length = Rev(reader.ReadUInt32());

			byte[] data = reader.ReadBytes((int)length);
			return new MsgStr(Encoding.UTF8.GetString(data));
		}

		public override object? Value => value;

		public static implicit operator string(MsgStr msg) => msg.value;

		public override string ToString() => $"\"{value}\""; // TODO: Escape

		public int Length => value.Length;

		public override bool Equals(object obj) => obj is MsgStr msg && this == msg;
		public static bool operator !=(MsgStr a, MsgStr b) => a.value != b.value;
		public static bool operator ==(MsgStr a, MsgStr b) => a.value == b.value;

		public override int GetHashCode()
		{
			return HashCode.Combine(value, 0x3177D334);
		}
	}
}
