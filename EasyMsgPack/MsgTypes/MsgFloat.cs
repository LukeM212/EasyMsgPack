﻿using System;
using System.IO;

namespace EasyMsgPack
{
	using static Helpers;

	public class MsgFloat : MsgNumber
	{
		private readonly double value;

		public MsgFloat(double value) => this.value = value;
		public MsgFloat(float value) => this.value = value;

		public static implicit operator MsgFloat(double value) => new MsgFloat(value);
		public static implicit operator MsgFloat(float value) => new MsgFloat(value);

		public static explicit operator MsgInt?(MsgFloat msg)
		{
			if (msg.value % 1 == 0 && long.MinValue < msg.value && msg.value < ulong.MaxValue)
			{
				if (msg.value >= 0) return new MsgInt((ulong)msg.value);
				else return new MsgInt((long)msg.value);
			}
			else return null;
		}

		public override void Pack(BinaryWriter stream)
		{
			if ((MsgInt?)this is MsgInt i) i.Pack(stream);
			else
			{
				if ((float)value == value || double.IsNaN(value))
				{
					stream.Write((byte)0xCA);
					stream.Write(Rev(BitConverter.SingleToInt32Bits((float)value)));
				}
				else
				{
					stream.Write((byte)0xCB);
					stream.Write(Rev(BitConverter.DoubleToInt64Bits((double)value)));
				}
			}
		}

		internal static Msg Unpack(byte type, BinaryReader reader)
		{
			if (type == 0xCA) return new MsgFloat(BitConverter.Int32BitsToSingle(Rev(reader.ReadInt32())));
			else return new MsgFloat(BitConverter.Int64BitsToDouble(Rev(reader.ReadInt64())));
		}

		public override object? Value => value;

		public static explicit operator sbyte(MsgFloat msg) => (sbyte)msg.value;
		public static explicit operator byte(MsgFloat msg) => (byte)msg.value;
		public static explicit operator short(MsgFloat msg) => (short)msg.value;
		public static explicit operator ushort(MsgFloat msg) => (ushort)msg.value;
		public static explicit operator int(MsgFloat msg) => (int)msg.value;
		public static explicit operator uint(MsgFloat msg) => (uint)msg.value;
		public static explicit operator long(MsgFloat msg) => (long)msg.value;
		public static explicit operator ulong(MsgFloat msg) => (ulong)msg.value;

		public static implicit operator double(MsgFloat msg) => msg.value;
		public static explicit operator float(MsgFloat msg) => (float)msg.value;

		public override string ToString() => value.ToString();

		public override bool Equals(object obj) => base.Equals(obj);
		public static bool operator !=(MsgFloat a, MsgFloat b) => a.value != b.value;
		public static bool operator ==(MsgFloat a, MsgFloat b) => a.value == b.value;

		public override int GetHashCode()
		{
			if ((MsgInt?)this is MsgInt i) return i.GetHashCode();
			else return HashCode.Combine(value, 0x40FA017E);
		}
	}
}
