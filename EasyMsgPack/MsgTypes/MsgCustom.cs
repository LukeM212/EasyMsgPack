﻿using System;
using System.Linq;

namespace EasyMsgPack
{
	public class MsgCustom : MsgExt
	{
		private readonly sbyte type;
		private readonly byte[] data;

		public MsgCustom(sbyte type, byte[] data)
		{
			this.type = type;
			this.data = data;
		}

		public override sbyte Type => type;
		public override byte[] Data => data;

		public override object? Value => throw new NotSupportedException();

		public override string ToString() => throw new NotSupportedException();

		public override bool Equals(object obj) => obj is MsgCustom msg && this == msg;
		public static bool operator !=(MsgCustom a, MsgCustom b) => !(a == b);
		public static bool operator ==(MsgCustom a, MsgCustom b)
		{
			return a.type == b.type && Enumerable.SequenceEqual(a.data, b.data);
		}

		public override int GetHashCode()
		{
			return data.Aggregate(
				HashCode.Combine(unchecked((int)0x4DB997CD), type),
				(total, next) => HashCode.Combine(total, next)
			);
		}
	}
}
