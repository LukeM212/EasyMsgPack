﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EasyMsgPack
{
	using static Helpers;

	public class MsgMap : Msg, IEnumerable<KeyValuePair<Msg, Msg>>
	{
		private readonly Dictionary<Msg, Msg> value;

		public MsgMap(Dictionary<Msg, Msg> value)
		{
			this.value = value;
		}

		public MsgMap() : this(new Dictionary<Msg, Msg>()) { }

		public static implicit operator MsgMap(Dictionary<Msg, Msg> value) => new MsgMap(value);

		public override void Pack(BinaryWriter stream)
		{
			uint length = (uint)value.Count;

			if (length <= 0b1111) stream.Write((byte)(length | 0b1000_0000));
			else if (length <= ushort.MaxValue)
			{
				stream.Write((byte)0xDE);
				stream.Write(Rev((ushort)length));
			}
			else
			{
				stream.Write((byte)0xDF);
				stream.Write(Rev((uint)length));
			}

			foreach ((Msg key, Msg value) in value)
			{
				key.Pack(stream);
				value.Pack(stream);
			}
		}

		internal static Msg Unpack(byte type, BinaryReader reader)
		{
			uint length;

			if (type <= 0b1000_1111) length = type & (uint)0b1111;
			else if (type == 0xDE) length = Rev(reader.ReadUInt16());
			else length = Rev(reader.ReadUInt32());

			Dictionary<Msg, Msg> content = new Dictionary<Msg, Msg>();
			for (uint i = 0; i < length; i++) {
				Msg key = Msg.Unpack(reader);
				Msg value = Msg.Unpack(reader);
				content.Add(key, value);
			}
			return new MsgMap(content);
		}

		public override object? Value => value;

		public Msg this[Msg key] => value[key];

		public static implicit operator Dictionary<Msg, Msg>(MsgMap msg) => msg.value;

		public override string ToString() => throw new NotImplementedException();

		public IEnumerator<KeyValuePair<Msg, Msg>> GetEnumerator() => value.GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => value.GetEnumerator();

		public void Add(Msg key, Msg value) => this.value.Add(key, value);
		public int Count => value.Count;

		public override bool Equals(object obj) => obj is MsgMap msg && this == msg;
		public static bool operator !=(MsgMap a, MsgMap b) => !(a == b);
		public static bool operator ==(MsgMap a, MsgMap b)
		{
			return Enumerable.SequenceEqual(
				a.value.OrderBy(kvp => kvp.Key.GetHashCode()),
				b.value.OrderBy(kvp => kvp.Key.GetHashCode())
			);
		}

		public override int GetHashCode()
		{
			return value
				.Select(kvp => (kvp.Key.GetHashCode(), kvp.Value.GetHashCode()))
				.OrderBy(kvp => kvp.Item1)
				.Aggregate(
					unchecked((int)0xF290E7FD),
					(total, next) => HashCode.Combine(total, next.Item1, next.Item2)
				);
		}
	}
}
