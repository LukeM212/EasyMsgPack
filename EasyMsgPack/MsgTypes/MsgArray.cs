﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EasyMsgPack
{
	using static Helpers;

	public class MsgArray : Msg, IEnumerable<Msg>
	{
		private readonly List<Msg> value;

		public MsgArray(List<Msg> value)
		{
			this.value = value.ToList();
		}

		public MsgArray(Msg[] value) : this(value.ToList()) { }

		public MsgArray() : this(new List<Msg>()) { }

		public static implicit operator MsgArray(List<Msg> value) => new MsgArray(value);
		public static implicit operator MsgArray(Msg[] value) => new MsgArray(value);

		public override void Pack(BinaryWriter stream)
		{
			uint length = (uint)value.Count;

			if (length <= 0b1111) stream.Write((byte)(length | 0b1001_0000));
			else if (length <= ushort.MaxValue)
			{
				stream.Write((byte)0xDC);
				stream.Write(Rev((ushort)length));
			}
			else
			{
				stream.Write((byte)0xDD);
				stream.Write(Rev((uint)length));
			}

			foreach (Msg msg in value) msg.Pack(stream);
		}

		internal static Msg Unpack(byte type, BinaryReader reader)
		{
			uint length;

			if (type <= 0b1001_1111) length = type & (uint)0b1111;
			else if (type == 0xDC) length = Rev(reader.ReadUInt16());
			else length = Rev(reader.ReadUInt32());

			Msg[] content = new Msg[length];
			for (uint i = 0; i < length; i++) content[i] = Msg.Unpack(reader);
			return new MsgArray(content);
		}

		public override object? Value => value;

		public Msg this[int index] => value[index];

		public static implicit operator List<Msg>(MsgArray msg) => msg.value;
		public static explicit operator Msg[](MsgArray msg) => msg.value.ToArray();

		public override string ToString() => throw new NotImplementedException();

		public IEnumerator<Msg> GetEnumerator() => ((IEnumerable<Msg>)value).GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => value.GetEnumerator();

		public void Add(params Msg[] values) => this.value.AddRange(values);
		public int Count => value.Count;

		public override bool Equals(object obj) => obj is MsgArray msg && this == msg;
		public static bool operator !=(MsgArray a, MsgArray b) => !(a == b);
		public static bool operator ==(MsgArray a, MsgArray b)
		{
			return Enumerable.SequenceEqual(a.value, b.value);
		}

		public override int GetHashCode()
		{
			return value.Aggregate(
				unchecked((int)0x9AE2864D),
				(total, next) => HashCode.Combine(total, next)
			);
		}
	}
}
