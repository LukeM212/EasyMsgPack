﻿using System;

namespace EasyMsgPack
{
	public class MsgTimestamp : MsgExt
	{
		private readonly DateTime value;

		public MsgTimestamp(DateTime value)
		{
			this.value = value;
		}

		public static implicit operator MsgTimestamp(DateTime value) => new MsgTimestamp(value);

		public override sbyte Type => -1;
		public override byte[] Data
		{
			get
			{
				long ticks = value.Ticks - DateTime.UnixEpoch.Ticks;

				long s = Helpers.DivMod(ticks, 10000000, out long remainder);
				uint ns = (uint)remainder * 100;

				if (ns == 0 && 0 <= s && s <= uint.MaxValue) return BitConverter.GetBytes((uint)s);
				else if (0 <= s && s <= 0b11_11111111_11111111_11111111_11111111) return BitConverter.GetBytes(((ulong)ns << 34) | (ulong)s);
				else
				{
					byte[] data = new byte[12];
					BitConverter.GetBytes((uint)ns).CopyTo(data, 0);
					BitConverter.GetBytes((long)s).CopyTo(data, 4);
					return data;
				}
			}
		}

		internal MsgTimestamp(sbyte type, byte[] data)
		{
			uint ns;
			long s;

			if (data.Length == 4)
			{
				ns = 0;
				s = BitConverter.ToUInt32(data);
			}
			else if (data.Length == 8)
			{
				ulong content = BitConverter.ToUInt64(data);
				ns = (uint)(content >> 34);
				s = (long)(content & 0b11_11111111_11111111_11111111_11111111);
			}
			else
			{
				ns = BitConverter.ToUInt32(data[0..4]);
				s = BitConverter.ToInt64(data[4..12]);
			}

			long ticks = (s * 10000000) + (ns / 100);
			value = new DateTime(ticks + DateTime.UnixEpoch.Ticks);
		}

		public override object? Value => value;

		public static implicit operator DateTime(MsgTimestamp msg) => msg.value;

		public override string ToString() => value.ToUniversalTime().ToString("'\"'yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z\"'");

		public override bool Equals(object obj) => obj is MsgTimestamp msg && this == msg;
		public static bool operator !=(MsgTimestamp a, MsgTimestamp b) => a.value != b.value;
		public static bool operator ==(MsgTimestamp a, MsgTimestamp b) => a.value == b.value;

		public override int GetHashCode()
		{
			return HashCode.Combine(value, 0x3177D334);
		}
	}
}
