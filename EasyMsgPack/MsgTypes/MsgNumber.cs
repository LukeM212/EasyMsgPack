﻿namespace EasyMsgPack
{
	public abstract class MsgNumber : Msg
	{
		public static explicit operator sbyte(MsgNumber msg) => msg is MsgInt ? (sbyte)(MsgInt)msg : (sbyte)(MsgFloat)msg;
		public static explicit operator byte(MsgNumber msg) => msg is MsgInt ? (byte)(MsgInt)msg : (byte)(MsgFloat)msg;
		public static explicit operator short(MsgNumber msg) => msg is MsgInt ? (short)(MsgInt)msg : (short)(MsgFloat)msg;
		public static explicit operator ushort(MsgNumber msg) => msg is MsgInt ? (ushort)(MsgInt)msg : (ushort)(MsgFloat)msg;
		public static explicit operator int(MsgNumber msg) => msg is MsgInt ? (int)(MsgInt)msg : (int)(MsgFloat)msg;
		public static explicit operator uint(MsgNumber msg) => msg is MsgInt ? (uint)(MsgInt)msg : (uint)(MsgFloat)msg;
		public static explicit operator long(MsgNumber msg) => msg is MsgInt ? (long)(MsgInt)msg : (long)(MsgFloat)msg;
		public static explicit operator ulong(MsgNumber msg) => msg is MsgInt ? (ulong)(MsgInt)msg : (ulong)(MsgFloat)msg;

		public static explicit operator float(MsgNumber msg) => msg is MsgInt ? (float)(MsgInt)msg : (float)(MsgFloat)msg;
		public static explicit operator double(MsgNumber msg) => msg is MsgInt ? (double)(MsgInt)msg : (double)(MsgFloat)msg;

		public override bool Equals(object obj) => obj is MsgNumber msg && this == msg;
		public static bool operator !=(MsgNumber a, MsgNumber b) => !(a == b);
		public static bool operator ==(MsgNumber a, MsgNumber b)
		{
			return a switch
			{
				MsgFloat af => b switch
				{
					MsgFloat bf => af == bf,
					MsgInt bi => (MsgInt?)af is MsgInt ai && ai == bi,
					_ => (object)a == (object)b,
				},
				MsgInt ai => b switch
				{
					MsgFloat bf => (MsgInt?)bf is MsgInt bi && ai == bi,
					MsgInt bi => ai == bi,
					_ => (object)a == (object)b,
				},
				_ => (object)a == (object)b,
			};
		}

		public override int GetHashCode() => base.GetHashCode();
	}
}
