﻿using System;
using System.IO;

namespace EasyMsgPack
{
	public class MsgBool : Msg
	{
		public static MsgBool True = new MsgBool(true);
		public static MsgBool False = new MsgBool(false);

		private readonly bool value;

		private MsgBool(bool value)
		{
			this.value = value;
		}

		public static implicit operator MsgBool(bool value) => value ? True : False;

		public override void Pack(BinaryWriter stream)
		{
			stream.Write(value ? (byte)0xC3 : (byte)0xC2);
		}

		internal static Msg Unpack(byte type, BinaryReader reader)
		{
			return type == 0xC3 ? True : False;
		}

		public override object? Value => value;

		public static implicit operator bool(MsgBool msg) => msg.value;

		public override string ToString() => value ? "true" : "false";

		public override bool Equals(object obj) => obj is MsgBool msg && this == msg;
		public static bool operator !=(MsgBool a, MsgBool b) => a.value != b.value;
		public static bool operator ==(MsgBool a, MsgBool b) => a.value == b.value;

		public override int GetHashCode()
		{
			return HashCode.Combine(value, 0xE52C2D47);
		}
	}
}
