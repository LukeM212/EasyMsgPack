﻿using System;
using System.IO;

namespace EasyMsgPack
{
	using static Helpers;

	public class MsgInt : MsgNumber
	{
		private readonly bool signed;
		private readonly long value;

		private MsgInt(bool signed, long value)
		{
			this.signed = signed;
			this.value = value;
		}

		public MsgInt(sbyte value) : this(true, (long)value) { }
		public MsgInt(byte value) : this(false, (long)value) { }
		public MsgInt(short value) : this(true, (long)value) { }
		public MsgInt(ushort value) : this(false, (long)value) { }
		public MsgInt(int value) : this(true, (long)value) { }
		public MsgInt(uint value) : this(false, (long)value) { }
		public MsgInt(long value) : this(true, (long)value) { }
		public MsgInt(ulong value) : this(false, (long)value) { }

		public static implicit operator MsgInt(sbyte value) => new MsgInt(value);
		public static implicit operator MsgInt(byte value) => new MsgInt(value);
		public static implicit operator MsgInt(short value) => new MsgInt(value);
		public static implicit operator MsgInt(ushort value) => new MsgInt(value);
		public static implicit operator MsgInt(int value) => new MsgInt(value);
		public static implicit operator MsgInt(uint value) => new MsgInt(value);
		public static implicit operator MsgInt(long value) => new MsgInt(value);
		public static implicit operator MsgInt(ulong value) => new MsgInt(value);

		public override void Pack(BinaryWriter stream)
		{
			if (signed && value < 0)
			{
				if (value >= -0b100000) stream.Write((sbyte)value);
				else if (value >= sbyte.MinValue)
				{
					stream.Write((byte)0xD0);
					stream.Write((sbyte)value);
				}
				else if (value >= short.MinValue)
				{
					stream.Write((byte)0xD1);
					stream.Write(Rev((short)value));
				}
				else if (value >= int.MinValue)
				{
					stream.Write((byte)0xD2);
					stream.Write(Rev((int)value));
				}
				else
				{
					stream.Write((byte)0xD3);
					stream.Write(Rev((long)value));
				}
			}
			else
			{
				ulong value = (ulong)this.value;

				if (value <= 0b1111111) stream.Write((byte)value);
				else if (value <= byte.MaxValue)
				{
					stream.Write((byte)0xCC);
					stream.Write((byte)value);
				}
				else if (value <= ushort.MaxValue)
				{
					stream.Write((byte)0xCD);
					stream.Write(Rev((ushort)value));
				}
				else if (value <= uint.MaxValue)
				{
					stream.Write((byte)0xCE);
					stream.Write(Rev((uint)value));
				}
				else
				{
					stream.Write((byte)0xCF);
					stream.Write(Rev((ulong)value));
				}
			}
		}

		internal static Msg Unpack(byte type, BinaryReader reader)
		{
			if (type <= 0b01111111) return new MsgInt((byte)type);
			else if (type <= 0xCF)
			{
				if (type == 0xCC) return new MsgInt(reader.ReadByte());
				else if (type == 0xCD) return new MsgInt(Rev(reader.ReadUInt16()));
				else if (type == 0xCE) return new MsgInt(Rev(reader.ReadUInt32()));
				else return new MsgInt(Rev(reader.ReadUInt64()));
			}
			else if (type <= 0xD3)
			{
				if (type == 0xD0) return new MsgInt(reader.ReadSByte());
				else if (type == 0xD1) return new MsgInt(Rev(reader.ReadInt16()));
				else if (type == 0xD2) return new MsgInt(Rev(reader.ReadInt32()));
				else return new MsgInt(Rev(reader.ReadInt64()));
			}
			else return new MsgInt((sbyte)type);
		}

		public override object? Value => signed && value < 0 ? (object)value : (object)(ulong)value;

		public static explicit operator sbyte(MsgInt msg) => (sbyte)msg.value;
		public static explicit operator byte(MsgInt msg) => (byte)msg.value;
		public static explicit operator short(MsgInt msg) => (short)msg.value;
		public static explicit operator ushort(MsgInt msg) => (ushort)msg.value;
		public static explicit operator int(MsgInt msg) => (int)msg.value;
		public static explicit operator uint(MsgInt msg) => (uint)msg.value;
		public static explicit operator long(MsgInt msg) => (long)msg.value;
		public static explicit operator ulong(MsgInt msg) => (ulong)msg.value;

		public static explicit operator double(MsgInt msg) => (double)msg.value;
		public static explicit operator float(MsgInt msg) => (float)msg.value;

		public override string ToString() => signed ? value.ToString() : ((ulong)value).ToString();

		public override bool Equals(object obj) => base.Equals(obj);
		public static bool operator !=(MsgInt a, MsgInt b) => a.value != b.value;
		public static bool operator ==(MsgInt a, MsgInt b) => a.value == b.value;

		public override int GetHashCode()
		{
			return HashCode.Combine(value, 0x0928A19A);
		}
	}
}
