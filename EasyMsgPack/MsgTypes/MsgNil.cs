﻿using System.IO;

namespace EasyMsgPack
{
	public class MsgNil : Msg
	{
		public static MsgNil Nil = new MsgNil();

		private MsgNil() { }

		public override void Pack(BinaryWriter stream)
		{
			stream.Write((byte)0xC0);
		}

		internal static Msg Unpack(byte type, BinaryReader reader)
		{
			return Nil;
		}

		public override object? Value => null;

		public override string ToString() => "null";

		public override bool Equals(object obj) => obj is MsgNil;
		public static bool operator !=(MsgNil a, MsgNil b) => false;
		public static bool operator ==(MsgNil a, MsgNil b) => true;

		public override int GetHashCode()
		{
			return unchecked((int)0xF01A297D);
		}
	}
}
